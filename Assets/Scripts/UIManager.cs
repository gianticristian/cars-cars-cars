﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {
	private Text lap;
	[SerializeField]
	private Button replay;
	[SerializeField]
	private Button mainMenu;
	[SerializeField]
	private GameObject buttons;


	// Use this for initialization
	void Start () 
	{
		buttons.SetActive (false);
		lap = GetComponentInChildren<Text>();
	}
	
	public string GetLap () 
	{
		return lap.text;
	}

	public void SetLap (int _lap) 
	{
		lap.text = "Lap: " + _lap.ToString();
	}

	public void ShowButtons ()
	{
		buttons.SetActive (true);
	}
	
}
