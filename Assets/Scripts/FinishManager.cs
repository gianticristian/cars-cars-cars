﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Vehicles.Car;
using UnityStandardAssets.Utility;

public class FinishManager : MonoBehaviour 
{
	public int totalLaps;
	public GameObject[] winOrder;

	private int pos = 0;
	public int Pos
	{
		get { return pos; }
	}

	void OnTriggerExit (Collider other) 
	{
		// Player
		if (other.tag == "Player") 
		{
			Car player = other.GetComponentInParent<Car>();
			player.SetLap(1);
			// End the race?
			if (player.GetLap() > totalLaps && player.GetEnd() == false) 
			{
				player.SetEnd(true);										// Set his end as true
				player.SetPosition(pos);									// Set his position
				player.StopMovement(winOrder[pos].transform);				// Put him in the platform
				pos++;														// Prepare 'pos' for the next car
			}
		}
		// Enemies
		if (other.GetComponentInParent<Transform>().GetComponentInParent<Transform>().tag == "Enemy") 
		{
			Enemy enemy = other.GetComponentInParent<Enemy>();				// Avoid 2 levels of 'GetComponentInParent'
			enemy.SetLap(1);												// Add a lap to this car
			// End the race?
			if (enemy.GetLap() > totalLaps && enemy.GetEnd() == false) 
			{
				enemy.SetEnd(true);											// Set his end as true
				enemy.SetPosition(pos);										// Set his position
				enemy.StopMovement(winOrder[pos].transform);				// Put him in the platform
				pos++;														// Prepare 'pos' for the next car
			}
		}
	}
}
