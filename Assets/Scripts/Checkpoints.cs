﻿using UnityEngine;
using System.Collections;

public class Checkpoints : MonoBehaviour {
	public Transform[] points;

	public Transform GetPoint (int _index) {
		return points[_index];
	}
	
}
