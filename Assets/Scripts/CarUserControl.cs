using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace UnityStandardAssets.Vehicles.Car {
    [RequireComponent(typeof (CarController))]
    public class CarUserControl : MonoBehaviour {
        private CarController m_Car; 							// The car controller we want to use
		private Car car;										// Reference to the other script

        private void Awake() {
			m_Car = GetComponent<CarController>();				// Get the car controller
			car = GetComponent<Car>();							// Get the car script
        }

		private void Update () {
			if (Input.GetKey(KeyCode.X)) {						// If the player press 'X'
				car.Respawn();									// Will respawn on the last safe checkpoint
			}
		}
        private void FixedUpdate() {
            // pass the input to the car!
            float h = CrossPlatformInputManager.GetAxis("Horizontal");
            float v = CrossPlatformInputManager.GetAxis("Vertical");
			#if !MOBILE_INPUT
            	float handbrake = CrossPlatformInputManager.GetAxis("Jump");
            	m_Car.Move(h, v, v, handbrake);
			#else
            	m_Car.Move(h, v, v, 0f);
			#endif
        }
    }
}
