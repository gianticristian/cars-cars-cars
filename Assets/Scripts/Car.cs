﻿using UnityEngine;
using System.Collections;
//using UnityStandardAssets.Vehicles.Car;

namespace UnityStandardAssets.Vehicles.Car {
	public class Car : MonoBehaviour {
		private int checkpoint = 0;									// The current checkpoint
		public int lap;												// The current lap, the first lap will be the "0"
		private int position = -1;
		private bool end;
		private Transform lastPoint;								// Position of that checkpoint
		private Animator animator;

		void Start () 
		{
			lastPoint = transform.root;								// Save the start position
			animator = GetComponent<Animator>();
		}

		public int GetCheckpoint () 
		{
			return checkpoint;
		}

		public void SetCheckpoint (int _checkpoint) 
		{
			checkpoint = _checkpoint;
		}

		public int GetLap () 
		{
			return lap;
		}

		public void SetLap (int _lap) 
		{
			lap += _lap;
		}

		public Transform GetLastPoint () 
		{
			return lastPoint;
		}

		public void SetLastPoint (Transform _point) 
		{
			lastPoint = _point;
		}

		public void Respawn () 
		{
			transform.root.GetComponent<CarController>().SetIsRespawning(true);  	// Disable the movement
			transform.position = lastPoint.position;								// Move to the last safe position
			transform.root.GetComponent<Rigidbody>().velocity = Vector3.zero;		// Set the velocity to zero
			animator.SetTrigger("IsRespawning");									// Play the "Respawn" animation
			transform.root.GetComponent<CarController>().SetIsRespawning(false);	// Enable the movement
			transform.rotation = lastPoint.rotation;								// Set the last safe rotation
		}

		public bool GetEnd () {
			return end;
		}
		
		public void SetEnd (bool _end) {
			end = _end;
		}
		
		public int GetPosition () {
			return position;
		}
		
		public void SetPosition (int _position) {
			position = _position;
		}

		public void StopMovement (Transform _platform) {
			// Disable the driving and sound scripts
			GetComponent<CarController>().enabled = false;
			GetComponent<CarUserControl>().enabled = false;
			GetComponent<CarAudio>().StopSound();
			GetComponent<CarAudio>().enabled = false;
			GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;		// Freeze movement on XYZ
			// Move the platform to the right, an animation will be better
			_platform.position = new Vector3(_platform.position.x + 4, _platform.position.y, _platform.position.z);
			transform.position = _platform.position;									// Move the car to the right platform
			// Move the car to the right to see number, this can be improved
			transform.position = new Vector3(transform.position.x + 3, transform.position.y, transform.position.z);
			transform.rotation = _platform.rotation;									// Set his rotation
		}
	}
}
