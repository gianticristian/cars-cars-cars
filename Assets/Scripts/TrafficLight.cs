﻿using UnityEngine;
using System.Collections;

public class TrafficLight : MonoBehaviour {
	public Color waitColor = Color.red;					// Wait color
	public Color readyColor = Color.green;				// Go color!
	public bool ready = false;							// Is the light green?
	public AudioClip soundWait;
	public AudioClip soundReady;
	private AudioSource sound;
	private Light lt; 


	// Use this for initialization
	void Start () {
		lt = GetComponent<Light>();
		lt.color = Color.clear;	
		sound = GetComponentInParent<AudioSource>();
		sound.clip = soundWait;
		StartCoroutine(Wait());
	}


	IEnumerator Wait () {
		yield return new WaitForSeconds(2);				// Wait for 2 seconds
		// 3
		lt.color = waitColor;
		PlaySound();
		yield return new WaitForSeconds(1);				// Wait for 5 seconds
		lt.color = Color.clear;	
		yield return new WaitForSeconds(2);				// Wait for 5 seconds
		// 2
		lt.color = waitColor;
		PlaySound();
		yield return new WaitForSeconds(1);				// Wait for 5 seconds
		lt.color = Color.clear;	
		yield return new WaitForSeconds(2);				// Wait for 5 seconds
		// Go!
		lt.color = readyColor;							// Change the color to green
		ChangeSound();
		PlaySound();
		ready = true;									// Set the light as ready!
		yield return new WaitForSeconds(5);
		// Turn off
		lt.color = Color.clear;							// Turn off the color of the light
	}

	public bool GetReady () {
		return ready;									// Return the current status of the light
	}

	public void PlaySound () {
		sound.Play();
	}

	public void ChangeSound () {
		sound.clip = soundReady;
	}
	



}
