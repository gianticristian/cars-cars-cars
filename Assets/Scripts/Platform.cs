﻿using UnityEngine;
using System.Collections;

public class Platform : MonoBehaviour {

	// Used to see the forward direction of the checkpoint

	void OnDrawGizmosSelected() {
		Gizmos.color = Color.red;
		Vector3 direction = transform.TransformDirection(Vector3.forward) * 10;
		Gizmos.DrawRay(transform.position, direction);
	}
	
}
