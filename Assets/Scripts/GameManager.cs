﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Vehicles.Car;									// Add this namespace from 'CarUserControl.cs'

public class GameManager : MonoBehaviour 
{
	public GameObject UI;
	public GameObject player;
	public Light lt;
	public GameObject[] enemies;
	[SerializeField]
	private FinishManager finishManager;
	private bool raceDone = false;


	// Use this for initialization
	void Start () 
	{
		player.GetComponent<CarController>().SetReady(false);			// Disable the movement until the light is green
		foreach (GameObject enemy in enemies) 						
		{							
			enemy.GetComponent<CarController>().SetReady(false);
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		Countdown();
		ManageUI();
		RaceDone ();
	}

	private void RaceDone ()
	{
		if (finishManager.Pos == enemies.Length + 1) 
		{
			raceDone = true;
			UI.GetComponent<UIManager> ().ShowButtons ();
		}
	}

	private void ManageUI () 
	{
		// Set the text of the UI
		if (player.GetComponent<Car>().GetLap() > 0) 					// This is not the first lap
		{					
			UI.GetComponent<UIManager>().SetLap(player.GetComponent<Car>().GetLap());
		}
		else 
		{
			UI.GetComponent<UIManager>().SetLap(0);						// The first lap will be "0"
		}
	}

	private void Countdown () 
	{
		// If the light is ready (green)
		if (lt.GetComponent<TrafficLight>().GetReady()) 
		{					
			player.GetComponent<CarController>().SetReady(true);		// Enable the movement
			foreach (GameObject enemy in enemies) 
			{						
				enemy.GetComponent<CarController>().SetReady(true);		// Run enemies, RUN!
			}
		}
	}
}
