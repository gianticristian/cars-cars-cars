﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Vehicles.Car;

public class Checkpoint : MonoBehaviour 
{	
	void OnTriggerEnter (Collider other) 
	{
		if (other.transform.root.tag == "Player") 
		{
			Car _player = other.transform.root.GetComponent<Car>();				
			Checkpoints _group = GetComponentInParent<Checkpoints>();
			// Hit the right checkpoint
			if (transform == _group.GetPoint(_player.GetCheckpoint())) 
			{		
				if (_player.GetCheckpoint() + 1 < _group.points.Length) 
				{		
					_player.SetCheckpoint(_player.GetCheckpoint() + 1);
					_player.SetLastPoint(transform);
				}
				// If the player hit all the checkpoints
				else  
				{
					_player.SetCheckpoint(0);									// Set it back to zero

				}
				GetComponentInParent<BoxCollider>().enabled = false;			// Disable the collider
			}
			// The player miss a checkpoint
			else 
			{
				_player.Respawn();												// Move him to the last correct checkpoint
			}
			StartCoroutine(Wait());												
		}
	}
	
	IEnumerator Wait () 
	{
		yield return new WaitForSeconds(5);										// Wait for 5 seconds
		GetComponentInParent<BoxCollider>().enabled = true;						// Enable the collider
	}

	// Used to see the forward direction of the checkpoint
	/*
	void OnDrawGizmosSelected() 
	{
		Gizmos.color = Color.red;
		Vector3 direction = transform.TransformDirection(Vector3.forward) * 5;
		Gizmos.DrawRay(transform.position, direction);
	}
	*/
}
