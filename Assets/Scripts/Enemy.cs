﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Utility;

namespace UnityStandardAssets.Vehicles.Car 
{
	public class Enemy : MonoBehaviour 
	{
		private int lap = 0;
		private int position = -1;
		private bool end;

		public int GetLap () 
		{
			return lap;
		}

		public void SetLap (int _lap) 
		{
			lap += _lap;
		}

		public bool GetEnd () {
			return end;
		}

		public void SetEnd (bool _end) 
		{
			end = _end;
		}

		public int GetPosition () 
		{
			return position;
		}
		
		public void SetPosition (int _position) 
		{
			position = _position;
		}

		public void StopMovement (Transform _platform) 
		{
			// Disable the driving and sound scripts
			GetComponent<CarController>().SetReady(false);
			GetComponent<CarController>().enabled = false;
			GetComponent<CarAIControl>().enabled = false;
			GetComponent<CarAudio>().StopSound();										// Stop all the AudioSource from this car
			GetComponent<CarAudio>().enabled = false;
			GetComponent<WaypointProgressTracker>().enabled = false;
			GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;		// Freeze movement on XYZ
			// Move the platform to the right, an animation will be better
			_platform.position = new Vector3(_platform.position.x + 4, _platform.position.y, _platform.position.z);
			transform.position = _platform.position;									// Move the car to the right platform
			// Move the car to the right to see number, this can be improved
			transform.position = new Vector3(transform.position.x + 4, transform.position.y, transform.position.z);
			transform.rotation = _platform.rotation;									// Set his rotation
		}
	}
}
