﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {
	public Transform target;
	public float distance;

	// Use this for initialization
	void Start () {
		// Carga por primera vez la posicion
		transform.position = target.position;
		transform.position = new Vector3(transform.position.x, transform.position.y + distance, transform.position.z);

	}
	
	// Update is called once per frame
	void Update () {
		// Actualiza la posicion
		transform.position = target.position;
		transform.position = new Vector3(transform.position.x, transform.position.y + distance, transform.position.z);
		transform.LookAt(target.position);
	}
}
