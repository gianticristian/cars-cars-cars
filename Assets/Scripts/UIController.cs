﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIController : MonoBehaviour 
{

	public void Play()
	{
		SceneManager.LoadScene ("Track 01");
	}

	public void Exit()
	{
		Application.Quit ();
	}

	public void Credits()
	{
		SceneManager.LoadScene ("Credits");
	}

	public void MainMenu()
	{
		SceneManager.LoadScene ("MainMenu");
	}

	public void Retry()
	{
		SceneManager.LoadScene ("Track 01");
	}
}
